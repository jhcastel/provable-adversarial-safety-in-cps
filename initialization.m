%--------------------------------------------------------------------------
% Based on initialization_ex1_6.m file of hybrid simulator project.
%--------------------------------------------------------------------------

% clear all   
SWaTData=false;
                                                                        
% initial conditions
if SWaTData 
    seed = 29000+3500;
    L1 = RawData.LIT101(seed);
    L2 = RawData.LIT301(seed);
    L3 = RawData.LIT401(seed);
    MV1 = RawData.MV101(seed)-1;
    P1 = RawData.P101(seed)-1;
    MV2 = RawData.MV201(seed)-1;
    P2 = RawData.P301(seed)-1;
    MV3 = RawData.MV302(seed)-1;
    P3 = RawData.P401(seed)-1;
else
    L1 = 821.7;
    L2 = 926.3;
    L3 = 953.3;
    MV1 = 0;
    P1 = 0;
    MV2 = 0;
    P2 = 0;
    MV3 = 0;
    P3 = 1;
end


x1_0 = [L1; 1; MV1; P1];
x2_0 = [L2; 1; MV2; P2];
x3_0 = [L3; 1; MV3; P3];

% simulation horizon                                                    
T = 3000;                                                                 
J = 20;                                                                 
                                                                        
% rule for jumps                                                        
% rule = 1 -> priority for jumps                                        
% rule = 2 -> priority for flows                                        
% rule = 3 -> no priority, random selection when simultaneous conditions
rule = 1;                     
    
% solver tolerances
RelTol = 1e-8;
MaxStep = 1.0;           

% attack
ta = 10000;