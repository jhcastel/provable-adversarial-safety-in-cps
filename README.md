# Provable Adversarial Safety in CPS

This repo contains the software and additional content (proofs) that were summarized from [our paper](https://doi.ieeecomputersociety.org/10.1109/EuroSP57164.2023.00062), _Provable Adversarial Safety in Cyber-Physical Systems_. 


## Model - A Matlab simulation

The SWaT simulation is coded as a Simulink program empowered by the [Hybrid System toolbox](https://hybrid.soe.ucsc.edu/software). 

The `initialization.m` script sets initial values for all sensors and actuators included in the simulation. Variables `T` and `J` set the simulation horizon for continuous and discrete variables, respectively.

After running the `SWaT_3tanks.slx` model, the variables x11, x21, and x31 store the resulting traces of tanks T1, T2, and T3.

## Complete mathematical proofs

The _Model and Proofs of Security for SWaT_ document includes the detailed testbed model with theorems and proofs.

---

## In proceedings of the IEEE 8th European Symposium on Security and Privacy (EuroS&P'23)

When using this repository please cite our work as follows:

```
@inproceedings{castellanos2023provable,
  title={Provable Adversarial Safety in Cyber-Physical Systems},
  author={Castellanos, John H and Maghenem, Mohamed and C{\'a}rdenas, Alvaro A and Sanfelice, Ricardo G and Zhou, Jianying},
  booktitle={2023 IEEE 8th European Symposium on Security and Privacy (EuroS\&P)},
  pages={979--1012},
  year={2023},
  organization={IEEE Computer Society}
}
```